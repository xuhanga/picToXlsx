package logic;

import java.util.List;

/**
 * Excel像素画生成器.
 * @author Rex
 *
 */
public class PicToXlsx {
	
	public static void main(String[] args) throws Exception {
		args = checkArgs(args);
		// list的容量为图片高度（行数），rgb数组的长度为图片宽度（列数）
		List<int[]> load = LoadPic.load(args[0]);
		// 填入到Excel
		CreateXlsx.create(load, args[1]);
	}
	
	/**
	 * 参数检查.
	 * @param args
	 * @throws Exception
	 */
	private static String[] checkArgs(final String[] args) throws Exception {
		if (args.length != 2)
			throw new Exception("Incorrect number of parameters.");

		args[0] = args[0].toLowerCase().trim();
		if (!(args[0].endsWith(".jpg") || args[0].endsWith(".png") || args[0].endsWith(".gif")
				|| args[0].endsWith(".bmp") || args[0].endsWith(".jpeg") || args[0].endsWith(".jpe")
				|| args[0].endsWith(".jfif")))
			throw new Exception("Image format error.");

		if (!(args[1] = args[1].toLowerCase().trim()).endsWith(".xlsx"))
			throw new Exception("The output file must be an xlsx file.");

		return args;
	}
	
}